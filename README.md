# RajatPatwari.Extensions
Extensions to the .NET Core Base Class Library.

### Notes
__Framework:__ .NET Core 3.1

__Author:__ Rajat Patwari

__License:__ BSD 3-Clause