﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RajatPatwari.Extensions.Collections.Generic
{
    public sealed class Glossary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        private int _index;

        private readonly KeyValuePair<TKey, TValue>[] _pairs;

        public int Length =>
            _pairs.Length;

        public TValue this[TKey key]
        {
            get => _pairs[GetIndexOfKey(key)].Value;
            set => _pairs[GetIndexOfKey(key)] = new KeyValuePair<TKey, TValue>(key, value);
        }

        public Glossary(int length) =>
            _pairs = new KeyValuePair<TKey, TValue>[length];

        public void Add(TKey key, TValue value)
        {
            if (ContainsKey(key))
                throw new ArgumentException($"{nameof(key)} is already in the collection!", nameof(key));
            if (_index > Length)
                throw new InvalidOperationException("Adding to a full collection!");

            _pairs[_index++] = new KeyValuePair<TKey, TValue>(key, value);
        }

        public void Clear()
        {
            Array.Clear(_pairs, 0, _pairs.Length);
            _index = 0;
        }

        public bool ContainsKey(TKey key) =>
            GetKeys().Contains(key);

        public bool ContainsValue(TValue value) =>
            GetValues().Contains(value);

        public TKey GetKeyAt(int index) =>
            _pairs[index].Key;

        public TValue GetValueAt(int index) =>
            _pairs[index].Value;

        public IEnumerable<TKey> GetKeys() =>
            _pairs.Select(pair => pair.Key);

        public IEnumerable<TValue> GetValues() =>
            _pairs.Select(pair => pair.Value);

        public int GetIndexOfKey(TKey key) =>
            Array.IndexOf(GetKeys().ToArray(), key);

        public int GetIndexOfValue(TValue value) =>
            Array.IndexOf(GetValues().ToArray(), value);

        public int GetLastIndexOfValue(TValue value) =>
            Array.LastIndexOf(GetValues().ToArray(), value);

        public void Reverse() =>
            Array.Reverse(_pairs);

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() =>
            (IEnumerator<KeyValuePair<TKey, TValue>>)_pairs.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() =>
            _pairs.GetEnumerator();
    }
}