﻿using System;

namespace RajatPatwari.Extensions
{
    public static class PipingExtensions
    {
        public static void Pipe<T>(this T argument, Action<T> method) =>
            method(argument);

        public static TResult Pipe<TArgument, TResult>(this TArgument argument, Func<TArgument, TResult> method) =>
            method(argument);
    }
}